package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class MediumCrustDough implements Dough {

    @Override
    public String toString() {
        return "Medium crust dough";
    }
}
