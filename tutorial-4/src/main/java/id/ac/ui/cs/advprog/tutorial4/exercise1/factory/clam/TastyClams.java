package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class TastyClams implements Clams {

    @Override
    public String toString() {
        return "Tasty clams from Margonda";
    }

}
