package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;


public class GoudaCheese implements Cheese {
    @Override
    public String toString() {
        return "Gouda cheese";
    }

}
