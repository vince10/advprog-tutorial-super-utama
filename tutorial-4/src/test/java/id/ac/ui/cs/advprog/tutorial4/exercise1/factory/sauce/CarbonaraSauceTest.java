package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CarbonaraSauceTest {
    private Sauce sauce;

    @Before
    public void setUp() {
        sauce = new CarbonaraSauce();
    }

    @Test
    public void testChiliSauceToStringMethod() {
        assertEquals(sauce.toString(), "Carbonara Sauce");
    }
}
