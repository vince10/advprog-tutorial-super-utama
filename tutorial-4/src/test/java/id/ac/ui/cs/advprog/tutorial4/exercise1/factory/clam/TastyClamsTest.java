package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TastyClamsTest {
    private Clams clams;

    @Before
    public void setUp() {
        clams = new TastyClams();
    }

    @Test
    public void testHotClamsToStringMethod() {
        assertEquals(clams.toString(), "Tasty clams from Margonda");
    }
}
