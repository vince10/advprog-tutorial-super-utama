package id.ac.ui.cs.advprog.tutorial1.strategy;


public class ModelDuck extends Duck{
    // TODO Complete me!
    public ModelDuck(){
        this.setQuackBehavior(new Quack());
        this.setFlyBehavior(new FlyNoWay());
    }

    @Override
    public void display() { System.out.println("I'm a model duck");}
}
