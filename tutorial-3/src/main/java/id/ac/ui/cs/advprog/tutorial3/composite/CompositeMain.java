package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

public class CompositeMain {

    public static void main(String args[]){
        Company Gojek = new Company();
        Gojek.addEmployee(new FrontendProgrammer("Messi",30001));
        Gojek.addEmployee(new BackendProgrammer("Cristiano",20001));
        Gojek.addEmployee(new NetworkExpert("Kylian",50001));
        Gojek.addEmployee(new SecurityExpert("Ramos",70001));
        Gojek.addEmployee(new UiUxDesigner("Pogboom",90001));
        Gojek.addEmployee(new Ceo("Mourinho",200001));
        Gojek.addEmployee(new Cto("Guardiola",100001));
        System.out.println(Gojek.getNetSalaries());
        for(Employees e: Gojek.getAllEmployees()){
            System.out.println(e.name+" kerjaannya sebagai "+e.role+" dibayar dengan gaji "+e.salary);
        }
    }
}
