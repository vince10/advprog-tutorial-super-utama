package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Lettuce;

public class DecoratorMain {

    public  static  void  main(String args[]){
        CrustySandwich crustySandwich =  new CrustySandwich();
        BeefMeat add_beef = new BeefMeat(crustySandwich);
        Cheese add_cheese = new Cheese(add_beef);
        Lettuce sayur_kol = new Lettuce(add_cheese);

        System.out.println(sayur_kol.getDescription()+" harganya "+sayur_kol.cost()+" dollar ");

    }
}
